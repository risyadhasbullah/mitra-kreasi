
/*
 * Editor client script for DB table mitrakreasi
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.mitrakreasi.php',
		table: '#mitrakreasi',
		fields: [
			{
				"label": "deskripsi:",
				"name": "deskripsi"
			}
		]
	} );

	var table = $('#mitrakreasi').DataTable( {
		dom: 'Bfrtip',
		ajax: 'php/table.mitrakreasi.php',
		columns: [
			{
				"data": "gambar"
			},
			{
				"data": "deskripsi"
			}
		],
		select: true,
		lengthChange: false,
		buttons: [
			{ extend: 'edit',   editor: editor }
		]
	} );
} );

}(jQuery));

