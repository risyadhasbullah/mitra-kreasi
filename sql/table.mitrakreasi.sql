-- 
-- Editor SQL for DB table mitrakreasi
-- Created by http://editor.datatables.net/generator
-- 

CREATE TABLE IF NOT EXISTS `mitrakreasi` (
	`id` int(10) NOT NULL auto_increment,
	`gambar` numeric(9,2),
	`deskripsi` varchar(255),
	PRIMARY KEY( `id` )
);